﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreLama : MonoBehaviour
{
    Text text;
    public static int nSecondi;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Controllo che il giocatrore sia vivo e stampo lo score sullo schermo
    void Update()
    {
        if (!GameManager.morto)
        {
            text.text = "Score: " + nSecondi;
        } else
        {
            text.text = "";
        }
    }
}
