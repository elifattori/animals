﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{

    public float JumpForce;
    bool CanJump;
    Rigidbody2D rb2d;
    public GameManager GMScript;
    public ScoreFinePartita score;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (CanJump)
        {
            if (Input.touchCount > 0)
            {
                rb2d.AddForce(Vector3.up * JumpForce, ForceMode2D.Impulse);
                CanJump = false;
            }
        }
        ScoreLama.nSecondi++;  //NON SONO SECONDI MA NON MI ANDAVA DI CONVERTIRE IL TIME.DELTATIME
        
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Land")
        {
            CanJump = true;
        }
        if(collision.transform.tag == "BadGuy")
        {
            gameObject.SetActive(false);  
            GMScript.GameLost(); 
            score.scoreFinePartita(); //Faccio partire la sequenza dello score 
            Debug.Log("Sei morto come un Bigonzo");
        }
    }
}
