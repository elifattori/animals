﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    [Header("Cactus Stuff")]
    public GameObject CactusPrefab;
    int nCactus = 10;
    public float CactusSpeed;

    [Header("Cactus Timer  Stuff")]
    [Range(0.1f,3f)]public float Timer;
    [Range(0.1f,1f)]public float MinTimer;
    [Range(0.1f,1f)]public float TimerMultiplier;
    float CactusTimerElapsed;

    Queue<GameObject> CactusPool;

    [Header("Bird Stuff")]
    public GameObject BirdPrefab;
    int nBird = 10;
    public float BirdSpeed;

    public Canvas menu;
    public static bool morto;

    Queue<GameObject> BirdPool;
    // Start is called before the first frame update
    void Start()
    {
        CactusTimerElapsed = 0;
        morto = false;

        CactusPool = new Queue<GameObject>();
        GameObject go;
        for (int i = 0; i < nCactus; i++)
        {
            go = Instantiate(CactusPrefab);
            go.transform.position = transform.position;

            go.GetComponent<MoveLeft>().Speed = CactusSpeed;

            CactusPool.Enqueue(go);
            go.SetActive(false);
        }

        BirdPool = new Queue<GameObject>();
        for (int i = 0; i < nBird; i++)
        {
            go = Instantiate(BirdPrefab);
            go.transform.position = transform.position;

            go.GetComponent<MoveLeft>().Speed = BirdSpeed;

            BirdPool.Enqueue(go);
            go.SetActive(false);
        }
    }

    private void SpawnCactus()
    {
        GameObject go = CactusPool.Dequeue();
        go.transform.position = transform.position;
        go.SetActive(true);
        CactusPool.Enqueue(go);

    }


    private void SpawnBird()
    {
        GameObject go = BirdPool.Dequeue();
        go.transform.position = transform.position;
        go.SetActive(true);
        BirdPool.Enqueue(go);

    }

    // Update is called once per frame
    void Update()
    {
        if (morto == false)
        {
            if (CactusTimerElapsed >= Timer)
            {

                int selector = Random.Range(0, 3);

                if (selector < 2)
                {
                    SpawnCactus();

                }
                else
                {
                    SpawnBird();
                }
                CactusTimerElapsed = 0;
                Timer = Timer <= MinTimer ? Timer : Timer * TimerMultiplier;
            }
            else
            {
                CactusTimerElapsed += Time.deltaTime;
            }
        }
    }
    public void GameLost()
    {
        morto = true;
        menu.gameObject.SetActive(true);
    }
}
