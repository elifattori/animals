﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreFinePartita : MonoBehaviour
{
    public Text text;
    public Text highScore;
    int score;

    private void Start()
    {
        text = GetComponent<Text>();
        highScore = GetComponent<Text>();
    }

    /**
     * Funzione che prende lo score attuale e lo mette a confronto con l'highscore e nel caso in cui è maggiore lo sostituisce. 
     * Finito ciò stampa sia lo score che l'highScore
     */
    public void scoreFinePartita()
    {
        score = ScoreLama.nSecondi;
        

        if (score > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", score);
            PlayerPrefs.Save();
        }
        highScore.text = "HighScore: " + PlayerPrefs.GetInt("HighScore").ToString();
        text.text = "Your Score: " + score;
        ScoreLama.nSecondi = 0;

    }
}
