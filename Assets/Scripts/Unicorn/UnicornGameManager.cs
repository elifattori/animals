﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnicornGameManager : MonoBehaviour
{
    [Header("Corvo Stuff")]
    public GameObject corvoPrefab;
    public int nCorvo = 10;
    Queue<GameObject> CorvoPool;

    [Header("Baloon Stuff")]
    public GameObject baloonPrefab;
    public int nBaloon = 10;
    Queue<GameObject> BaloonPool;

    [Range(0.1f, 3f)] public float Timer;
    [Range(0.1f, 1f)] public float MinTimer;
    [Range(0.1f, 1f)] public float TimerMultiplier;
    float timeElapsed;

    public float speed;
    private new Vector2 camera;
    public Canvas menu;

    // Start is called before the first frame update
    
    void Start()
    {
        timeElapsed = 0;
        camera = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, -5));

        BaloonPool = new Queue<GameObject>();
        CorvoPool = new Queue<GameObject>();
        GameObject go;
        for (int i = 0; i < nCorvo; i++)
        {
            go = Instantiate(corvoPrefab);
            go.transform.position = transform.position;

            go.GetComponent<MoveLeft>().Speed = speed;

            CorvoPool.Enqueue(go);
            go.SetActive(false);
        }

        for (int i = 0; i < nBaloon; i++) 
        {
            go = Instantiate(baloonPrefab);
            go.transform.position = transform.position;

            go.GetComponent<MoveLeft>().Speed = speed;

            BaloonPool.Enqueue(go);
            go.SetActive(false);
        }
        Debug.Log(camera.y);
        Debug.Log(-camera.y);


    }

    private void spawnGoodGuy()
    {
        GameObject go = BaloonPool.Dequeue();
        go.transform.position = new Vector3(camera.x, Random.Range(-((float)camera.y), (float)camera.y), -2);
        go.SetActive(true);
        BaloonPool.Enqueue(go);

    }

    public void spawnBadGuy() 
    {
        GameObject go = CorvoPool.Dequeue();
        go.transform.position = new Vector3(camera.x, Random.Range(-((float)camera.y-10f), (float)camera.y-10f), -2);
        go.SetActive(true);
        CorvoPool.Enqueue(go);
    }
    // Update is called once per frame
    void Update()
    {
        int selector = Random.Range(0, 100);
        if (timeElapsed >= Timer) {
            //Spawno con una percentuale del 70% un gameObject cattivo e col 30 uno buono
            if(selector >= 70)
            {
                spawnBadGuy();
                
            } else
            {
                spawnGoodGuy();
            }

            timeElapsed = 0;
            Timer = Timer <= MinTimer ? Timer : Timer * TimerMultiplier;
        } 
        else 
        {
            timeElapsed += Time.deltaTime;
        }
    }
    public void GameLost()
    {
       menu.gameObject.SetActive(true);
    }
}
