﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
   
    Text score;
    public static int nCollisioni;
    
    private void Start()
    {
        nCollisioni = 0;
        score = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!UnicornJump.morto)
        {
            score.text = "Score: " + nCollisioni;
        } else
        {
            score.text = "";
        }
    }
}
