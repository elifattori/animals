﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnicornJump : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody2D rb;
    public float jumpForce;
    public UnicornGameManager UniGM;
    public ScoreFinePartitaUnicorn score;
    public static bool morto;
    void Start() 
    {
        morto = false;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        if(Input.touchCount > 0) { 
            rb.AddForce(Vector3.up * jumpForce, ForceMode2D.Impulse);
        }
    }

    public void OnCollisionEnter2D(Collision2D other) 
    {
        if (other.transform.tag == "BadGuy" || other.transform.tag == "Land") {
            morto = true;
            gameObject.SetActive(false);
            score.scoreFinePartita();
            Debug.Log("collisione");
            UniGM.GameLost();
        }
        else
        {
            //Se il player colpisce un GoodGuy aumenta il punteggio e disabilita il GoodGuy
            other.gameObject.SetActive(false);
            Score.nCollisioni++;
        }
    }
}
