﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreFinePartitaUnicorn : MonoBehaviour
{
    public Text score;
    public Text highScore;
    int punteggio;
    // Start is called before the first frame update
    void Start()
    {
        score = GetComponent<Text>();
        highScore = GetComponent<Text>();
    }

    // Update is called once per frame
    public void scoreFinePartita()
    {
        punteggio = Score.nCollisioni;


        if (punteggio > PlayerPrefs.GetInt("HighScoreUni"))
        {
            PlayerPrefs.SetInt("HighScoreUni", punteggio);
            PlayerPrefs.Save();
        }
        highScore.text = "HighScore: " + PlayerPrefs.GetInt("HighScoreUni").ToString();
        score.text = "Your Score: " + punteggio;
        Score.nCollisioni = 0;
    }
}
