﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeft : MonoBehaviour
{
    [HideInInspector]public float Speed;
    void Update()
    {
        transform.position -= transform.right * Speed * Time.deltaTime;
    }
}
